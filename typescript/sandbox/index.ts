interface User {
    name: string,
    age: number
}

const user: User = { name: 'Paulo', age: 34 }

user.name = 'hello';

