function rotate(point, angle) {
    const rad = angle * (Math.PI / 180);
    const cos = Math.cos(rad);
    const sin = Math.sin(rad);
    return {
        x: (point.x * cos) - (point.y * sin),
        y: (point.x * sin) + (point.y * cos)
    };
}

const proportion = rotate({ x: 1, y: 0 }, 45).x;

function hasQueenCollision(queen, size, col = { row: 0, diag1: 0, diag2: 0 }) {
    const row = Math.pow(2, queen.y);
    const rotQ = rotate(queen, 45);
    const s = size - 1;
    const propX = (rotQ.x / proportion + s);
    const propY = (rotQ.y / proportion + s);
    const diag1 = Math.pow(2, parseInt(propX, 10));
    const diag2 = Math.pow(2, parseInt(propY, 10));
    const hasCollision = Boolean((row & col.row) || (diag1 & col.diag1) ||
        (diag2 & col.diag2));

    return {
        hasCollision,
        collision: hasCollision ? col : {
            row: col.row | row,
            diag1: col.diag1 | diag1,
            diag2: col.diag2 | diag2
        }
    };
}

function nQueens(size, results, acc = [ ], col) {
    const x = acc.length;

    for (let y = 0; y < size; y++) {
        const res = hasQueenCollision({ x, y }, size, col);
        if (res.hasCollision) { continue; }

        const next = acc.concat(y);

        if (x === size - 1) { return results.push(next); }

        nQueens(size, results, next, res.collision);
    }
}

function printBoards(size, results) {
    for (let b of results) {
        console.log(b.join(' '));
        for (let x = 0; x < size; x++) {
            const line = [ ];
            for (let y = 0; y < size; y++) {
                const c = b[x] === y ? 'Q' : '.';
                line.push(c);
            }
            console.log(line.join(' '));
        }
        console.log('\n\n');
    }
}

const size = parseInt(process.argv[2], 10) || 8;
const results = [ ];
const dateStart = new Date();
nQueens(size, results);
const dateEnd = new Date();
printBoards(size, results);
console.log(`Took ${dateEnd.getTime() - dateStart.getTime()} ms`);
