mod sort;

fn main() {
    let mut args:Vec<String> = std::env::args().skip(1).collect();

    sort::sort_selection(&mut args);

    println!("{}", args.join(" - "));
}
