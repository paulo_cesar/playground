use std::cmp::Ordering;

pub fn sort_selection<T: Ord>(list: &mut Vec<T>) {
    let size = list.len();

    for i in 0..(size - 1) {
        for j in i..size {
            let mut smallest = i;
            match list[i].cmp(&list[j]) {
                Ordering::Greater => smallest = j,
                _ => (),
            }

            if i != smallest {
                list.swap(i, smallest);
            }
        }
    }
}

pub fn sort_insertion<T: Ord>(list: &mut Vec<T>) {
    for i in 1..list.len() {
        let mut pos = i;
        for j in (0..i).rev() {
            match list[i].cmp(&list[j]) {
                Ordering::Less => pos = j,
                _ => break,
            }
        }

        if pos != i {
            let v = list.remove(i);
            list.insert(pos, v);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn new_list() -> Vec<usize> { vec![4, 3, 2, 5, 1] }

    fn test_list(list: & Vec<usize>) {
        assert!(list[0] == 1);
        assert!(list[1] == 2);
        assert!(list[2] == 3);
        assert!(list[3] == 4);
        assert!(list[4] == 5);
    }

    #[test]
    fn should_sort_selection() {
        let mut list = new_list();

        sort_selection(&mut list);

        test_list(& list);
    }

    #[test]
    fn should_sort_insertion() {
        let mut list = new_list();

        sort_insertion(&mut list);

        test_list(& list);
    }
}
