#include <stdlib.h>
#include <stdio.h>
#include "data-structures.h"

int main(int argc, char ** argv) {
    char * str = NULL;

    if (argc > 1) { str = argv[1]; }

    if (str == NULL) {
        printf("\nplease provide a string\n\n");
        return 1;
    }

    charNode * linkedList = list_linked_create(str);

    printf("\nlist_linked_create\n");
    list_linked_print(linkedList);

    printf("\nlist_linked_shift\n");
    charNode * first = list_linked_shift(&linkedList);

    char_node_print(first);
    list_linked_print(linkedList);

    printf("\nlist_linked_unshift\n");
    list_linked_unshift(&linkedList, first);
    first = NULL;
    list_linked_print(linkedList);

    free(linkedList);

    printf("\n");
    return 0;
}
