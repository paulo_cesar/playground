#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "data-structures.h"

charNode * char_node_create(char c) {
    charNode * n = (charNode *) malloc(sizeof(charNode));

    n->value = c;
    n->pointers = NULL;

    return n;
}

void char_node_print(charNode * cn) {
    printf("%c -> %s\n", cn->value, cn->pointers != NULL ? "[Next]" : "NULL");
}

charNode * list_linked_create(char * str) {
    if (str == NULL) {
       return (charNode *) NULL;
    }

    int size = strlen(str);

    charNode * start = NULL;
    charNode * last = NULL;

    for (int i = 0; i < size; i++) {
        charNode * n = char_node_create(str[i]);

        if (start == NULL) {
            start = n;
            last = n;
            continue;
        }

        last->pointers = n;
        last = n;
    }

    return start;
}

void list_linked_print(charNode *list) {
    charNode * current = list;

    while (current != NULL) {
        printf("%c -> ", current->value);
        current = current->pointers;
    }
    printf("NULL\n");
}

charNode * list_linked_shift(charNode **list) {
    if (list == NULL) { return NULL; }

    charNode * first = *list;
    charNode * next = first->pointers;

    first->pointers = NULL;
    *list = next;

    return first;
}

void list_linked_unshift(charNode **list, charNode * first) {
    if (list == NULL || first == NULL) { return; }

    first->pointers = *list;
    *list = first;
}
