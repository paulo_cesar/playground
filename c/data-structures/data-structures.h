/*
 * list
 * linked list
 * stack
 * linked stack
 * queue
 * linked queue
 */

#ifndef __DATA_STRUCTURES_H_
#define __DATA_STRUCTURES_H_

typedef struct node {
    char value;
    struct node *pointers;
} charNode;

charNode * char_node_create(char c);
void char_node_print(charNode * cn);


charNode * list_linked_create(char * dftStr);

void list_linked_print(charNode *list);

charNode * list_linked_shift(charNode **list);
void list_linked_unshift(charNode **list, charNode * first);

#endif
